package grpc

import (
	"context"
	"gitee.com/doxlab/utivice/model"
	pb "gitee.com/doxlab/utivice/proto"
)

type Category interface {
	GetCategory(id string) (*model.Category, error)
	GetPath(id string) ([]*model.CategoryInfo, error)
}

type CategoryService struct {
	uc *model.CategoryUseCase
}

func NewCategoryService() *CategoryService {
	s := new(CategoryService)
	s.uc = model.NewCategoryUseCase()
	return s
}

func (o *CategoryService) GetCategory(ctx context.Context, rq *pb.CategoryRequest) (*pb.Category, error) {
	c, e := o.uc.Get(rq.Id)
	if e != nil {
		return nil, e
	}
	pc := &pb.Category{}
	pc.Id = c.ID
	pc.Name = c.Name
	pc.Code = c.Code
	pc.Module = c.Module
	pc.Parent = c.Parent
	return pc, nil

}
func (o *CategoryService) GetPath(ctx context.Context, rq *pb.CategoryRequest) (*pb.CategoryPath, error) {
	c, e := o.uc.GetPath(rq.Id)
	if e != nil {
		return nil, e
	}
	pc := make([]*pb.CategoryInfo, len(c))
	for i := 0; i < len(c); i++ {
		node := new(pb.CategoryInfo)
		node.Id = c[i].ID
		node.Name = c[i].Name
		node.Code = c[i].Code
		pc[i] = node
	}
	return &pb.CategoryPath{Info: pc}, nil

}
