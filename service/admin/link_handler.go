package admin

import (
	"encoding/json"
	"errors"
	"net/http"
	"strings"

	"gitee.com/doxlab/deflib"
	"gitee.com/doxlab/utivice/model"
	"github.com/atomtree/binding"
	"github.com/gorilla/mux"
	"github.com/unrolled/render"
)

//LinkHandler RESTful方法
// Model: Link
// Handler: Link
type LinkHandler struct {
	uc     *model.LinkUseCase
	render *render.Render
}

// NewLinkHandler 创建LinkHandler
func NewLinkHandler() *LinkHandler {
	return &LinkHandler{
		uc:     model.NewLinkUseCase(),
		render: jRender,
	}
}

// Get 根据ID获取单个链接管理功能信息
// RESTful:
// Path: /link/{id}
// Title: 获取单个链接管理功能
// Method: GET
// Response: Link
func (h *LinkHandler) Get(w http.ResponseWriter, r *http.Request) {
	rs := &Result{}
	defer HandleResult(rs, w)

	params := mux.Vars(r)
	id := params["id"]
	if id != "" {
		if data, err := h.uc.Get(id); err != nil {
			rs.Status = http.StatusNotFound
			rs.Err = err
		} else {
			rs.Data = data
		}
	} else {
		rs.Status = http.StatusBadRequest
		rs.Err = model.ErrorEmptyID
	}
}

// Query 查询链接管理功能信息
// RESTful:
// Path: /link
// Title: 查询链接管理功能
// Method: GET
// Param: QueryParam
// Response: {total:int,data:[]Link}
func (h *LinkHandler) Query(w http.ResponseWriter, r *http.Request) {
	rs := &Result{}
	defer HandleResult(rs, w)

	ql := new(model.LinkQL)
	if err := binding.Bind(r, ql); err != nil {
		rs.Status = http.StatusBadRequest
		rs.Err = errors.New("绑定查询参数出错:" + err.Error())
		return
	}
	// 检验输入
	if err := Validator.Struct(ql); err != nil {
		rs.Status = http.StatusBadRequest
		rs.Err = err
		return
	}
	if data, total, err := h.uc.Query(ql.Q()); err != nil {
		rs.Status = http.StatusInternalServerError
		rs.Err = errors.New("获取记录出错:" + err.Error())
	} else {
		rs.Data = deflib.M{
			"total": total,
			"data":  data,
		}
	}
}

// Post 新增链接管理功能记录
// RESTful:
// Path: /link
// Title: 新增链接管理功能
// Method: POST
// Param: Link
// Response: Link
func (h *LinkHandler) Post(w http.ResponseWriter, r *http.Request) {

	rs := &Result{}
	defer HandleResult(rs, w)

	entity := model.Link{}
	if err := json.NewDecoder(r.Body).Decode(&entity); err != nil {
		rs.Status = http.StatusBadRequest
		rs.Err = errors.New("绑定查询参数出错:" + err.Error())
		return
	}
	// 检验输入
	if err := Validator.Struct(entity); err != nil {
		rs.Status = http.StatusBadRequest
		rs.Err = err
		return
	}
	if err := h.uc.Create(&entity); err != nil {
		rs.Status = http.StatusInternalServerError
		rs.Err = err
	} else {
		rs.Data = entity
	}
}

// Put 修改链接管理功能记录
// RESTful:
// Path: /link/{id}
// Title: 修改链接管理功能
// Method: PUT
// Param: Link
// Response: Link
func (h *LinkHandler) Put(w http.ResponseWriter, r *http.Request) {

	rs := &Result{}
	defer HandleResult(rs, w)

	params := mux.Vars(r)
	id := params["id"]

	entity := model.Link{}
	if err := json.NewDecoder(r.Body).Decode(&entity); err != nil {
		rs.Status = http.StatusBadRequest
		rs.Err = errors.New("绑定查询参数出错:" + err.Error())
		return
	}
	entity.ID = id
	// 检验输入
	if err := Validator.Struct(entity); err != nil {
		rs.Status = http.StatusBadRequest
		rs.Err = err
		return
	}
	if err := h.uc.Update(&entity); err != nil {
		rs.Status = http.StatusInternalServerError
		rs.Err = err
	} else {
		rs.Data = entity
	}
}

// Delete 删除链接管理功能记录
// RESTful:
// Path: /link/{id}
// Title: 删除链接管理功能
// Method: DELETE
// Param: id
// Response: ok
func (h *LinkHandler) Delete(w http.ResponseWriter, r *http.Request) {

	rs := &Result{}
	defer HandleResult(rs, w)

	params := mux.Vars(r)
	id := params["id"]
	data, _, err := h.uc.Query(deflib.QueryParam{
		Filter: deflib.M{
			"id": deflib.M{"$in": strings.Split(id, ",")},
		},
	})
	if err != nil {
		rs.Err = err
		return
	}

	errs := make([]string, 0, 0)
	for _, v := range data {
		if err := h.uc.Delete(&v); err != nil {
			errs = append(errs, err.Error())
		}
	}
	if len(errs) > 0 {
		rs.Status = http.StatusInternalServerError
		rs.Err = errors.New("delete error")
		rs.Data = errs
	} else {
		rs.Data = "success"
	}
}

// UpdateExtend 设置扩展属性
// RESTful:
// Path: /link/{id}/extend
// Title: 设置扩展属性
// Method: PUT
// Param: model.map[string]interface{}
// Response: model.map[string]interface{}
// Doc:
func (h *LinkHandler) UpdateExtend(w http.ResponseWriter, r *http.Request) {
	rs := &Result{}
	defer HandleResult(rs, w)

	id := mux.Vars(r)["id"]
	if id == "" {
		rs.Status = http.StatusBadRequest
		rs.Err = model.ErrorEmptyID
		return
	}

	extend := make(map[string]interface{})
	if err := json.NewDecoder(r.Body).Decode(&extend); err != nil {
		rs.Status = http.StatusBadRequest
		rs.Err = err
		return
	}

	// 检验输入
	if err := Validator.Struct(extend); err != nil {
		rs.Status = http.StatusBadRequest
		rs.Err = err
		return
	}
	if err := h.uc.SetExtend(id, extend); err != nil {
		rs.Status = http.StatusInternalServerError
		rs.Err = err
	} else {
		rs.Data = extend
	}

}
