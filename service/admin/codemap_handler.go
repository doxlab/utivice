package admin

import (
	"encoding/json"
	"errors"
	"gitee.com/doxlab/deflib"
	"gitee.com/doxlab/utivice/model"
	"github.com/atomtree/binding"
	"github.com/gorilla/mux"
	"github.com/unrolled/render"
	"net/http"
	"strings"
)

//CodeMapHandler RESTful方法
// Model: CodeMap
// Handler: CodeMap
type CodeMapHandler struct {
	uc     *model.CodeMapUseCase
	render *render.Render
}

// NewCodeMapHandler 创建CodeMapHandler
func NewCodeMapHandler() *CodeMapHandler {
	return &CodeMapHandler{
		uc:     model.NewCodeMapUseCase(),
		render: jRender,
	}
}

// Get 根据ID获取单个代码表信息
// RESTful:
// Path: /code/{id}
// Title: 获取单个代码表
// Method: GET
// Response: CodeMap
func (h *CodeMapHandler) Get(w http.ResponseWriter, r *http.Request) {
	rs := &Result{}
	defer HandleResult(rs, w)

	params := mux.Vars(r)
	id := params["id"]
	if id != "" {
		if data, err := h.uc.Get(id); err != nil {
			rs.Status = http.StatusNotFound
			rs.Err = err
		} else {
			rs.Data = data
		}
	} else {
		rs.Status = http.StatusBadRequest
		rs.Err = model.ErrorEmptyID
	}
}

// GetByKey 根据key获取单个代码表信息
// RESTful:
// Path: /code/select/{key}
// Title: 获取单个代码表
// Method: GET
// Response: CodeMap
func (h *CodeMapHandler) GetByKey(w http.ResponseWriter, r *http.Request) {
	rs := &Result{}
	defer HandleResult(rs, w)

	params := mux.Vars(r)
	id := params["key"]
	if id != "" {
		if data, err := h.uc.GetBykey(id); err != nil {
			rs.Status = http.StatusNotFound
			rs.Err = err
		} else {
			rs.Data = data
		}
	} else {
		rs.Status = http.StatusBadRequest
		rs.Err = model.ErrorEmptyID
	}
}

// Query 查询代码表信息
// RESTful:
// Path: /code
// Title: 查询代码表
// Method: GET
// Param: QueryParam
// Response: {total:int,data:[]CodeMap}
func (h *CodeMapHandler) Query(w http.ResponseWriter, r *http.Request) {
	rs := &Result{}
	defer HandleResult(rs, w)

	ql := new(model.CodeMapQL)
	if err := binding.Bind(r, ql); err != nil {
		rs.Status = http.StatusBadRequest
		rs.Err = errors.New("绑定查询参数出错:" + err.Error())
		return
	}
	// 检验输入
	if err := Validator.Struct(ql); err != nil {
		rs.Status = http.StatusBadRequest
		rs.Err = err
		return
	}
	if data, total, err := h.uc.Query(ql.Q()); err != nil {
		rs.Status = http.StatusInternalServerError
		rs.Err = errors.New("获取记录出错:" + err.Error())
	} else {
		rs.Data = deflib.M{
			"total": total,
			"data":  data,
		}
	}
}

// Post 新增代码表记录
// RESTful:
// Path: /code
// Title: 新增代码表
// Method: POST
// Param: CodeMap
// Response: CodeMap
func (h *CodeMapHandler) Post(w http.ResponseWriter, r *http.Request) {

	rs := &Result{}
	defer HandleResult(rs, w)

	entity := model.CodeMap{}
	if err := json.NewDecoder(r.Body).Decode(&entity); err != nil {
		rs.Status = http.StatusBadRequest
		rs.Err = errors.New("绑定查询参数出错:" + err.Error())
		return
	}
	// 检验输入
	if err := Validator.Struct(entity); err != nil {
		rs.Status = http.StatusBadRequest
		rs.Err = err
		return
	}
	if err := h.uc.Create(&entity); err != nil {
		rs.Status = http.StatusInternalServerError
		rs.Err = err
	} else {
		rs.Data = entity
	}
}

// Put 修改代码表记录
// RESTful:
// Path: /code/{id}
// Title: 修改代码表
// Method: PUT
// Param: CodeMap
// Response: CodeMap
func (h *CodeMapHandler) Put(w http.ResponseWriter, r *http.Request) {

	rs := &Result{}
	defer HandleResult(rs, w)

	params := mux.Vars(r)
	id := params["id"]

	entity := model.CodeMap{}
	if err := json.NewDecoder(r.Body).Decode(&entity); err != nil {
		rs.Status = http.StatusBadRequest
		rs.Err = errors.New("绑定查询参数出错:" + err.Error())
		return
	}
	entity.ID = id
	// 检验输入
	if err := Validator.Struct(entity); err != nil {
		rs.Status = http.StatusBadRequest
		rs.Err = err
		return
	}
	if err := h.uc.Update(&entity); err != nil {
		rs.Status = http.StatusInternalServerError
		rs.Err = err
	} else {
		rs.Data = entity
	}
}

// Delete 删除代码表记录
// RESTful:
// Path: /code/{id}
// Title: 删除代码表
// Method: DELETE
// Param: id
// Response: ok
func (h *CodeMapHandler) Delete(w http.ResponseWriter, r *http.Request) {

	rs := &Result{}
	defer HandleResult(rs, w)

	params := mux.Vars(r)
	id := params["id"]
	data, _, err := h.uc.Query(deflib.QueryParam{
		Filter: deflib.M{
			"id": deflib.M{"$in": strings.Split(id, ",")},
		},
	})
	if err != nil {
		rs.Err = err
		return
	}

	errs := make([]string, 0, 0)
	for _, v := range data {
		if err := h.uc.Delete(&v); err != nil {
			errs = append(errs, err.Error())
		}
	}
	if len(errs) > 0 {
		rs.Status = http.StatusInternalServerError
		rs.Err = errors.New("delete error")
		rs.Data = errs
	} else {
		rs.Data = "success"
	}
}

// PushCode 添加代码项
// RESTful:
// Path: /code/{id}/code
// Title: 添加代码项
// Method: POST
// Param: CodeItem
// Response: []CodeItem
// Doc:
func (h *CodeMapHandler) PushCode(w http.ResponseWriter, r *http.Request) {

	rs := &Result{}
	defer HandleResult(rs, w)

	if id := mux.Vars(r)["id"]; id == "" {
		rs.Status = http.StatusBadRequest
		rs.Err = model.ErrorEmptyID

	} else {
		entity := model.CodeItem{}
		if err := json.NewDecoder(r.Body).Decode(&entity); err != nil {
			rs.Status = http.StatusBadRequest
			rs.Err = err
			return
		}
		// 检验输入
		if err := Validator.Struct(entity); err != nil {
			rs.Status = http.StatusBadRequest
			rs.Err = err
			return
		}
		if err := h.uc.PushCode(id, entity); err != nil {
			rs.Status = http.StatusInternalServerError
			rs.Err = err
			return
		}
		if o, err := h.uc.Get(id); err != nil {
			rs.Status = http.StatusInternalServerError
			rs.Err = err
		} else {
			rs.Data = o.Code
		}

	}
}

// PullCode 删除代码项
// RESTful:
// Path: /code/{id}/code
// Title: 删除代码项
// Method: DELETE
// Param: id
// Response: []CodeItem
// Doc:
func (h *CodeMapHandler) PullCode(w http.ResponseWriter, r *http.Request) {
	rs := &Result{}
	defer HandleResult(rs, w)

	if id := mux.Vars(r)["id"]; id == "" {
		rs.Status = http.StatusBadRequest
		rs.Err = model.ErrorEmptyID
	} else {
		_ = r.ParseForm()
		did := r.FormValue("id")
		if did == "" {
			rs.Status = http.StatusBadRequest
			rs.Err = model.ErrorEmptyID
			return
		}
		if err := h.uc.PullCode(id, did); err != nil {
			rs.Status = http.StatusInternalServerError
			rs.Err = err
			return
		}
		if o, err := h.uc.Get(id); err != nil {
			rs.Status = http.StatusInternalServerError
			rs.Err = err
		} else {
			rs.Data = o.Code
		}

	}
}

// GetCode 获取代码项
// RESTful:
// Path: /code/{id}/code
// Title: 获取代码项
// Method: GET
// Param:
// Response: []CodeItem
// Doc:
func (h *CodeMapHandler) GetCode(w http.ResponseWriter, r *http.Request) {
	rs := &Result{}
	defer HandleResult(rs, w)

	if id := mux.Vars(r)["id"]; id == "" {
		rs.Status = http.StatusBadRequest
		rs.Err = model.ErrorEmptyID
	} else {
		if o, err := h.uc.Get(id); err != nil {
			rs.Status = http.StatusInternalServerError
			rs.Err = err
		} else {
			rs.Data = o
		}

	}
}

// Check 检查代码表是否存在
// RESTful:
// Path: /code/action/check
// Title: 检查代码表是否存在
// Method: POST
// Param: {key:codekey}
// Response: 返回true，则代码表已经存在；false则不存在
// Doc:
func (h *CodeMapHandler) Check(w http.ResponseWriter, r *http.Request) {

	rs := &Result{}
	defer HandleResult(rs, w)

	var param struct {
		Id string `json:"key"`
	}
	if err := json.NewDecoder(r.Body).Decode(&param); err != nil {

		rs.Status = http.StatusBadRequest
		rs.Err = err
		return
	}

	if param.Id == "" {

		rs.Status = http.StatusBadRequest
		rs.Err = errors.New("请输入key")
		return
	}

	exist := h.uc.Exist(param.Id)
	_ = h.render.JSON(w, http.StatusOK, exist)
}
