package admin

import (
	"encoding/json"
	"errors"
	"gitee.com/doxlab/deflib"
	"gitee.com/doxlab/utivice/model"
	"github.com/atomtree/binding"
	"github.com/gorilla/mux"
	"github.com/unrolled/render"
	"net/http"
	"strings"
)

//CategoryHandler RESTful方法
// Model: Category
// Handler: Category
type CategoryHandler struct {
	uc     *model.CategoryUseCase
	render *render.Render
}

// NewCategoryHandler 创建CategoryHandler
func NewCategoryHandler() *CategoryHandler {
	return &CategoryHandler{
		uc:     model.NewCategoryUseCase(),
		render: jRender,
	}
}

// Get 根据ID获取单个公共分类信息
// RESTful:
// Path: /category/{module}/{id}
// Title: 获取单个公共分类
// Method: GET
// Response: Category
func (h *CategoryHandler) Get(w http.ResponseWriter, r *http.Request) {
	rs := &Result{}
	defer HandleResult(rs, w)

	params := mux.Vars(r)
	id := params["id"]
	module := params["module"]
	if module == "" {
		rs.Status = http.StatusBadRequest
		rs.Err = model.ErrorEmptyModule
		return
	}
	if id != "" {
		if data, err := h.uc.Get(id); err != nil {
			rs.Status = http.StatusNotFound
			rs.Err = err
		} else {
			rs.Data = data
		}
	} else {
		rs.Status = http.StatusBadRequest
		rs.Err = model.ErrorEmptyID
	}
}

// Query 查询公共分类信息
// RESTful:
// Path: /category/{module}
// Title: 查询公共分类
// Method: GET
// Param: QueryParam
// Response: {total:int,data:[]Category}
func (h *CategoryHandler) Query(w http.ResponseWriter, r *http.Request) {
	rs := &Result{}
	defer HandleResult(rs, w)

	params := mux.Vars(r)
	module := params["module"]
	if module == "" {
		rs.Status = http.StatusBadRequest
		rs.Err = model.ErrorEmptyModule
		return
	}

	ql := new(model.CategoryQL)
	ql.Module = module
	if err := binding.Bind(r, ql); err != nil {
		rs.Status = http.StatusBadRequest
		rs.Err = errors.New("绑定查询参数出错:" + err.Error())
		return
	}
	// 检验输入
	if err := Validator.Struct(ql); err != nil {
		rs.Status = http.StatusBadRequest
		rs.Err = err
		return
	}
	if data, total, err := h.uc.Query(ql.Q()); err != nil {
		rs.Status = http.StatusInternalServerError
		rs.Err = errors.New("获取记录出错:" + err.Error())
	} else {
		rs.Data = deflib.M{
			"total": total,
			"data":  data,
		}
	}
}

// Post 新增公共分类记录
// RESTful:
// Path: /category/{module}
// Title: 新增公共分类
// Method: POST
// Param: Category
// Response: Category
func (h *CategoryHandler) Post(w http.ResponseWriter, r *http.Request) {

	rs := &Result{}
	defer HandleResult(rs, w)
	params := mux.Vars(r)
	module := params["module"]
	if module == "" {
		rs.Status = http.StatusBadRequest
		rs.Err = model.ErrorEmptyModule
		return
	}

	entity := model.Category{}
	entity.Module = module
	if err := json.NewDecoder(r.Body).Decode(&entity); err != nil {
		rs.Status = http.StatusBadRequest
		rs.Err = errors.New("绑定查询参数出错:" + err.Error())
		return
	}
	// 检验输入
	if err := Validator.Struct(entity); err != nil {
		rs.Status = http.StatusBadRequest
		rs.Err = err
		return
	}
	if err := h.uc.Create(&entity); err != nil {
		rs.Status = http.StatusInternalServerError
		rs.Err = err
	} else {
		rs.Data = entity
	}
}

// Put 修改公共分类记录
// RESTful:
// Path: /category/{module}/{id}
// Title: 修改公共分类
// Method: PUT
// Param: Category
// Response: Category
func (h *CategoryHandler) Put(w http.ResponseWriter, r *http.Request) {

	rs := &Result{}
	defer HandleResult(rs, w)

	params := mux.Vars(r)
	id := params["id"]

	entity := model.Category{}
	if err := json.NewDecoder(r.Body).Decode(&entity); err != nil {
		rs.Status = http.StatusBadRequest
		rs.Err = errors.New("绑定查询参数出错:" + err.Error())
		return
	}
	entity.ID = id
	// 检验输入
	if err := Validator.Struct(entity); err != nil {
		rs.Status = http.StatusBadRequest
		rs.Err = err
		return
	}
	if err := h.uc.Update(&entity); err != nil {
		rs.Status = http.StatusInternalServerError
		rs.Err = err
	} else {
		rs.Data = entity
	}
}

// Delete 删除公共分类记录
// RESTful:
// Path: /category/{module}{id}
// Title: 删除公共分类
// Method: DELETE
// Param: id
// Response: ok
func (h *CategoryHandler) Delete(w http.ResponseWriter, r *http.Request) {

	rs := &Result{}
	defer HandleResult(rs, w)

	params := mux.Vars(r)
	id := params["id"]
	data, _, err := h.uc.Query(deflib.QueryParam{
		Filter: deflib.M{
			"id": deflib.M{"$in": strings.Split(id, ",")},
		},
	})
	if err != nil {
		rs.Err = err
		return
	}

	errs := make([]string, 0, 0)
	for _, v := range data {
		if err := h.uc.Delete(&v); err != nil {
			errs = append(errs, err.Error())
		}
	}
	if len(errs) > 0 {
		rs.Status = http.StatusInternalServerError
		rs.Err = errors.New("delete error")
		rs.Data = errs
	} else {
		rs.Data = "success"
	}
}

// UpdateParent 设置上级分类
// RESTful:
// Path: /category/{module}/{id}/parent
// Title: 设置上级分类
// Method: PUT
// Param: { parent:string }
// Response: string
// Doc:
func (h *CategoryHandler) UpdateParent(w http.ResponseWriter, r *http.Request) {
	rs := &Result{}
	defer HandleResult(rs, w)

	id := mux.Vars(r)["id"]
	if id == "" {
		rs.Status = http.StatusBadRequest
		rs.Err = model.ErrorEmptyID
		return
	}
	var parent struct {
		Parent string `json:"parent"`
	}
	if err := json.NewDecoder(r.Body).Decode(&parent); err != nil {
		rs.Status = http.StatusBadRequest
		rs.Err = err
		return
	}

	// 检验输入
	if err := Validator.Struct(parent); err != nil {
		rs.Status = http.StatusBadRequest
		rs.Err = err
		return
	}
	if err := h.uc.SetParent(id, parent.Parent); err != nil {
		rs.Status = http.StatusInternalServerError
		rs.Err = err
	} else {
		rs.Data = parent
	}

}

// UpdateProperties 设置扩展属性
// RESTful:
// Path: /category/{module}/{id}/properties
// Title: 设置扩展属性
// Method: PUT
// Param: { properties:interface{} }
// Response: interface{}
// Doc:
func (h *CategoryHandler) UpdateProperties(w http.ResponseWriter, r *http.Request) {
	rs := &Result{}
	defer HandleResult(rs, w)

	id := mux.Vars(r)["id"]
	if id == "" {
		rs.Status = http.StatusBadRequest
		rs.Err = model.ErrorEmptyID
		return
	}
	var properties struct {
		Properties interface{} `json:"properties"`
	}
	if err := json.NewDecoder(r.Body).Decode(&properties); err != nil {
		rs.Status = http.StatusBadRequest
		rs.Err = err
		return
	}

	// 检验输入
	if err := Validator.Struct(properties); err != nil {
		rs.Status = http.StatusBadRequest
		rs.Err = err
		return
	}
	if err := h.uc.SetProperties(id, properties.Properties); err != nil {
		rs.Status = http.StatusInternalServerError
		rs.Err = err
	} else {
		rs.Data = properties
	}

}
