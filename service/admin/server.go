package admin

import (
	"net/http"

	"gitee.com/doxlab/infrastructure/ver"
	"gitee.com/doxlab/infrastructure/webserver"
	"github.com/go-playground/validator/v10"
)

const UserCtxKey = "user"

// Validator 全局校验器
var Validator *validator.Validate

// initValidator 初始化全局校验器
func initValidator() {
	Validator = validator.New()
}

func NewServer(cert, key, port string, gzipEnable bool, auth webserver.Authentication) *webserver.Server {
	server := webserver.NewClassic("Utivice", port, auth, gzipEnable, cert, key)
	// 初始化映射的路径
	Init(server)
	return server
}

/** Init 初始化映射的路径
 * server *webserver.Server 服务器
 * prefix string 路径前缀, 可以为空
 */
func Init(server *webserver.Server, prefix ...string) {
	p := ""
	if len(prefix) > 0 {

		p = prefix[0]
	}

	initValidator()
	server.Map_(p+"/ver", ver.Version)

	handlerCategory := NewCategoryHandler()
	server.MapMethods(p+"/category/{module}", handlerCategory.Query, http.MethodGet)
	server.MapMethods(p+"/category/{module}", handlerCategory.Post, http.MethodPost)
	server.MapMethods(p+"/category/{module}/{id}", handlerCategory.Put, http.MethodPut)
	server.MapMethods(p+"/category/{module}/{id}", handlerCategory.Get, http.MethodGet)
	server.MapMethods(p+"/category/{module}/{id}", handlerCategory.Delete, http.MethodDelete)
	server.MapMethods(p+"/category/{module}/{id}/parent", handlerCategory.UpdateParent, http.MethodPost)
	server.MapMethods(p+"/category/{module}/{id}/properties", handlerCategory.UpdateProperties, http.MethodPost)

	handlerCodeMap := NewCodeMapHandler()
	server.MapMethods(p+"/code", handlerCodeMap.Query, http.MethodGet)
	server.MapMethods(p+"/code", handlerCodeMap.Post, http.MethodPost)
	server.MapMethods(p+"/code/{id}", handlerCodeMap.Put, http.MethodPut)
	server.MapMethods(p+"/code/{id}", handlerCodeMap.Get, http.MethodGet)
	server.MapMethods(p+"/code/{id}", handlerCodeMap.Delete, http.MethodDelete)
	server.MapMethods(p+"/code/{id}/code", handlerCodeMap.PushCode, http.MethodPost)
	server.MapMethods(p+"/code/{id}/code", handlerCodeMap.PullCode, http.MethodDelete)
	server.MapMethods(p+"/code/{id}/code", handlerCodeMap.GetCode, http.MethodGet)
	server.MapMethods(p+"/code/select/{key}", handlerCodeMap.GetByKey, http.MethodGet)
	server.MapMethods(p+"/code/action/check", handlerCodeMap.Check, http.MethodPost)

	handlerLink := NewLinkHandler()
	server.MapMethods(p+"/link", handlerLink.Query, http.MethodGet)
	server.MapMethods(p+"/link", handlerLink.Post, http.MethodPost)
	server.MapMethods(p+"/link/{id}", handlerLink.Put, http.MethodPut)
	server.MapMethods(p+"/link/{id}", handlerLink.Get, http.MethodGet)
	server.MapMethods(p+"/link/{id}", handlerLink.Delete, http.MethodDelete)
	server.MapMethods(p+"/link/{id}/extend", handlerLink.UpdateExtend, http.MethodPost)
}
