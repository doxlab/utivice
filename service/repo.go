package service

import (
	"gitee.com/doxlab/infrastructure/repository"
	"gitee.com/doxlab/utivice/db"
	"gitee.com/doxlab/utivice/model"
)

func Repo(option repository.DaoOption) {
	// TODO: Modify

	model.Injector.Register(model.CategoryUseCase{}, db.NewCategoryRepo(option))

	model.Injector.Register(model.CodeMapUseCase{}, db.NewCodeMapRepo(option))

	model.Injector.Register(model.LinkUseCase{}, db.NewLinkRepo(option))
}
