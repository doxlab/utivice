FROM alpine

COPY ./bin/utivice /cmd/
COPY ./res /cmd/res

WORKDIR /cmd

ENTRYPOINT ["./utivice"]
