# 公共分类

 提供公共的分类功能，可以供整个系统使用。对于需要有分类的功能的应用模块，不需要自己开发一个分类功能。
只需要引用关联本功能模块的分类即可


## 数据项


### 公共分类 Category

 提供公共的分类功能，可以供整个系统使用。对于需要有分类的功能的应用模块，不需要自己开发一个分类功能。
只需要引用关联本功能模块的分类即可


| 数据项         | 名称       | 类型              | 必填 | 说明                           |
| -------------- | ---------- | ----------------- | -------- | -------------------------------------- |
| ID | id | string |  | |
| 创建时间 | createTime | time.Time |  | 系统自动修改设置|
| 修改时间 | updateTime | time.Time |  | 系统自动修改设置|
| 关联的功能模块 | module | string |  | |
| 分组 | group | string |  | |
| 分类名称 | name | string |  | |
| 分类编码 | code | string |  | |
| 说明 | description | string |  | |
| 上级分类 | parent | string |  | |
| 扩展属性 | properties | interface{} |  | |




## API



| Path                      | 名称           | 方式   | 参数             | 返回        | 说明 |
| ------------------------- | -------------- | ------ | ---------------- | ----------- | ---- |
| /category/{id} | 获取单个公共分类 | GET |  | Category ||
| /category | 查询公共分类 | GET | QueryParam | {total:int,data:[]Category} ||
| /category | 新增公共分类 | POST | Category | Category ||
| /category/{id} | 修改公共分类 | PUT | Category | Category ||
| /category/{id} | 删除公共分类 | DELETE | id | ok ||
| /category/{id}/parent | 设置上级分类 | PUT | { parent:string } | string ||
| /category/{id}/properties | 设置扩展属性 | PUT | { properties:interface{} } | interface{} ||

