# 代码表




## 数据项


### 代码表 CodeMap




| 数据项         | 名称       | 类型              | 必填 | 说明                           |
| -------------- | ---------- | ----------------- | -------- | -------------------------------------- |
| ID | id | string |  | |
| 创建时间 | createTime | time.Time |  | 系统自动修改设置|
| 修改时间 | updateTime | time.Time |  | 系统自动修改设置|
| 代码表编码 | codeKey | string |  | |
| 代码表名称 | name | string |  | |
| 说明 | description | string |  | |
| 代码项 | code | []CodeItem |  | |




### 代码项 CodeItem




| 数据项         | 名称       | 类型              | 必填 | 说明                           |
| -------------- | ---------- | ----------------- | -------- | -------------------------------------- |
| Label | label | string |  | |
| Value | value | string |  | |
| Summary | summary | string |  | |




## API



| Path                      | 名称           | 方式   | 参数             | 返回        | 说明 |
| ------------------------- | -------------- | ------ | ---------------- | ----------- | ---- |
| /code/{id} | 获取单个代码表 | GET |  | CodeMap ||
| /code/select/{key} | 获取单个代码表 | GET |  | CodeMap ||
| /code | 查询代码表 | GET | QueryParam | {total:int,data:[]CodeMap} ||
| /code | 新增代码表 | POST | CodeMap | CodeMap ||
| /code/{id} | 修改代码表 | PUT | CodeMap | CodeMap ||
| /code/{id} | 删除代码表 | DELETE | id | ok ||
| /code/{id}/code | 添加代码项 | POST | CodeItem | []CodeItem ||
| /code/{id}/code | 删除代码项 | DELETE | id | []CodeItem ||
| /code/{id}/code | 获取代码项 | GET |  | []CodeItem ||
| /code/action/check | 检查代码表是否存在 | POST | {key:codekey} | 返回true，则代码表已经存在；false则不存在 ||

