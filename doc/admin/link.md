# 链接管理功能

 提供一个公共的功能，管理系统的链接


## 数据项


### 链接管理功能 Link

 提供一个公共的功能，管理系统的链接


| 数据项         | 名称       | 类型              | 必填 | 说明                           |
| -------------- | ---------- | ----------------- | -------- | -------------------------------------- |
| ID | id | string |  | |
| 链接的名称 | label | string |  | |
| 图标的样式类名 | iconCls | string |  | |
| 图片图标 | image | string |  | |
| 地址 | url | string |  | |
| 标签 | tags | []string |  | |
| 扩展属性 | extend | map[string]interface{} |  | |
| 说明 | description | string |  | |
| 创建时间 | createTime | time.Time |  | 系统自动修改设置|
| 修改时间 | updateTime | time.Time |  | 系统自动修改设置|




## API



| Path                      | 名称           | 方式   | 参数             | 返回        | 说明 |
| ------------------------- | -------------- | ------ | ---------------- | ----------- | ---- |
| /link/{id} | 获取单个链接管理功能 | GET |  | Link ||
| /link | 查询链接管理功能 | GET | QueryParam | {total:int,data:[]Link} ||
| /link | 新增链接管理功能 | POST | Link | Link ||
| /link/{id} | 修改链接管理功能 | PUT | Link | Link ||
| /link/{id} | 删除链接管理功能 | DELETE | id | ok ||
| /link/{id}/extend | 设置扩展属性 | PUT | model.map[string]interface{} | model.map[string]interface{} ||

