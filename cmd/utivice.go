package main

import (
	"fmt"
	"log"
	"net"
	"path/filepath"

	"gitee.com/doxlab/auth-rpc/client"
	"gitee.com/doxlab/infrastructure/conf"
	"gitee.com/doxlab/infrastructure/repository"
	pb "gitee.com/doxlab/utivice/proto"
	"gitee.com/doxlab/utivice/service"
	"gitee.com/doxlab/utivice/service/admin"
	gserv "gitee.com/doxlab/utivice/service/grpc"

	"google.golang.org/grpc"
)

func main() {
	go func() {
		rpc()
	}()
	var cfg struct {
		Mongodb   repository.DaoOption `yaml:"mongodb"`
		WebServer struct {
			Port       string `yaml:"port"`
			Cert       string `yaml:"cert"`
			Key        string `yaml:"key"`
			GzipEnable bool   `yaml:"gzip_enable"`
		} `yaml:"web_server"`
		AdminAuth struct {
			Host string `yaml:"host"`
			Port string `yaml:"port"`
		} `yaml:"adminAuth"`
	}

	cfg.Mongodb = repository.DaoOption{
		Addr:     "mongodb://localhost:27017",
		Database: "kernel",
	}

	if err := conf.LoadConfigFromYaml("res/conf/conf.yml", &cfg); err != nil {
		log.Fatal(err)
	}
	service.Repo(cfg.Mongodb)

	var cert string
	var key string
	if cfg.WebServer.Cert != "" && cfg.WebServer.Key != "" {
		cert = filepath.Join("res", "assets", "server", cfg.WebServer.Cert)
		key = filepath.Join("res", "assets", "server", cfg.WebServer.Key)
	}
	auth := client.NewAdminAuthClient("user", cfg.AdminAuth.Host, cfg.AdminAuth.Port)
	server := admin.NewServer(cert, key, cfg.WebServer.Port, cfg.WebServer.GzipEnable, auth)
	log.Println("start Utivice server")
	server.Start()
}

func rpc() {
	var cfg struct {
		Mongodb repository.DaoOption `yaml:"mongodb"`
		RpcPort string               `yaml:"rpc_port"`
	}
	cfg.Mongodb = repository.DaoOption{
		Addr:     "mongodb://localhost:27017",
		Database: "kernel",
	}
	cfg.RpcPort = "9527"

	if err := conf.LoadConfigFromYaml("res/conf/conf.yml", &cfg); err != nil {
		log.Fatal(err)
	}

	service.Repo(cfg.Mongodb)
	lis, err := net.Listen("tcp", fmt.Sprintf(":%s", cfg.RpcPort))
	if err != nil {
		log.Fatalf("failed to listen: %v", err)
	}

	server := grpc.NewServer()
	// TODO: 修改下面初始化方法
	pb.RegisterCategoryServiceServer(server, gserv.NewCategoryService())
	log.Printf("starting Utivice grpc server at pogitrt[%s]...\n", cfg.RpcPort)
	if err := server.Serve(lis); err != nil {
		log.Fatal(err)
	}
}
