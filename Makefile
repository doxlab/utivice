PWD = $(shell pwd)
version=1.0
commit=`git log -1 --pretty=format:'%H'`
build=`git log -1 --pretty=format:'%H %cd manual' --date='format:%Y%m%d_%H%M'`
image=`date +%Y%m%d_%H%M`
time=`date +%D%t%T`
branch=`git rev-parse --abbrev-ref HEAD`
system=`lsb_release -a`
# tags=`git tag --contains=$(commit)`

version:
	-mkdir res
	echo "service: Utivice" > res/version.yml
	echo "version: $(version)" >> res/version.yml
	echo "build: $(build)" >> res/version.yml
	echo "branch: $(branch)" >> res/version.yml
	echo "time: $(time)" >> res/version.yml
	echo "sha: $(commit) " >> res/version.yml
	echo "$(system)" >> res/version.yml
	cat res/version.yml

build: version command image

build-ci: version ci

ci:
	-rm -fr ./bin/*
	CGO_ENABLED=0 GOOS=linux GOARCH=amd64 go build -o bin/bin/utivice cmd/utivice.go
	cp -r ./res ./bin 
	cp Dockerfile ./bin


protoc:
	protoc -I proto/ proto/${target} --go_out=plugins=grpc:proto

proto_auth:
	protoc -I proto/ proto/auth.proto --go_out=plugins=grpc:proto


command:
	CGO_ENABLED=0 GOOS=linux GOARCH=amd64 go build -o bin/utivice cmd/utivice.go

image:
	docker build -f Dockerfile -t utivice:latest .
	docker save -o $(PRDSTORE)/service/utivice.img utivice:latest
	docker save -o $(PRDSTORE)/service/utivice_$(image).img utivice:latest
	
sync-doc:
	rsync -p -r ./doc $(PRDSTORE)/atom-wiki/service/utivice