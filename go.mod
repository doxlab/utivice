module gitee.com/doxlab/utivice

go 1.12

require (
	gitee.com/doxlab/auth-rpc v0.0.0-20210527124029-6202e16cc9b7
	gitee.com/doxlab/deflib v0.0.0-20210204033819-0c44ad4f991a
	gitee.com/doxlab/infrastructure v0.0.0-20210422150734-1ce6c8d30ea3
	github.com/ajg/form v1.5.1 // indirect
	github.com/atomtree/binding v0.3.0
	github.com/brianvoe/gofakeit/v5 v5.11.2
	github.com/fasthttp-contrib/websocket v0.0.0-20160511215533-1f3b11f56072 // indirect
	github.com/fatih/structs v1.1.0 // indirect
	github.com/gavv/httpexpect v2.0.0+incompatible
	github.com/go-playground/validator/v10 v10.5.0
	github.com/golang/protobuf v1.5.2
	github.com/google/go-querystring v1.1.0 // indirect
	github.com/gorilla/mux v1.8.0
	github.com/gorilla/websocket v1.4.2 // indirect
	github.com/imkira/go-interpol v1.1.0 // indirect
	github.com/k0kubun/colorstring v0.0.0-20150214042306-9440f1994b88 // indirect
	github.com/leodido/go-urn v1.2.1 // indirect
	github.com/mattn/go-colorable v0.1.8 // indirect
	github.com/moul/http2curl v1.0.0 // indirect
	github.com/onsi/gomega v1.12.0 // indirect
	github.com/sergi/go-diff v1.2.0 // indirect
	github.com/smartystreets/goconvey v1.6.4 // indirect
	github.com/unrolled/render v1.0.3
	github.com/valyala/fasthttp v1.23.0 // indirect
	github.com/xeipuuv/gojsonschema v1.2.0 // indirect
	github.com/yalp/jsonpath v0.0.0-20180802001716-5cc68e5049a0 // indirect
	github.com/yudai/gojsondiff v1.0.0 // indirect
	github.com/yudai/golcs v0.0.0-20170316035057-ecda9a501e82 // indirect
	github.com/yudai/pp v2.0.1+incompatible // indirect
	go.mongodb.org/mongo-driver v1.5.1
	google.golang.org/grpc v1.37.0
)
