package test

import (
	"testing"

	"github.com/brianvoe/gofakeit/v5"
)

// TestLink 测试链接管理功能应用服务
func TestLink(t *testing.T) {

	_, server, e, auth := createTestServer(t)
	defer server.Close()

	token, err := auth.Login("admin", "123456")
	if err != nil {
		t.Fatal(err)
		return
	}
	token = "Bearer " + token
	path := "/admin/link"

	name := gofakeit.Name()
	entity := map[string]interface{}{
		// TODO Change Next Code
		"label":   name,
		"iconCls": "abc",
		"url":     "http://www.163.com",
	}

	// POST
	result := e.POST(path).
		WithHeader("Authorization", token).
		WithJSON(entity).Expect().Status(200).
		JSON().Object()
	result.Value("id").NotNull()
	id := result.Value("id").Raw().(string)
	// TODO Change Equal Test
	result.Value("label").Equal(name)

	// Query
	array := e.GET(path).
		WithQuery("l", 10).
		WithHeader("Authorization", token).Expect().Status(200).
		JSON().Object().Value("data").Array()
	array.Length().Gt(0)

	// Get
	result = e.GET(path+"/"+id).
		WithHeader("Authorization", token).Expect().Status(200).
		JSON().Object()
	result.Value("id").NotNull()
	// TODO Change Equal Test
	result.Value("label").Equal(name)
	//Put
	name = gofakeit.Name()
	entity["label"] = name
	result = e.PUT(path+"/"+id).
		WithHeader("Authorization", token).
		WithJSON(entity).Expect().Status(200).
		JSON().Object()
	result.Value("id").NotNull()
	id = result.Value("id").Raw().(string)
	// TODO Change Equal Test
	result.Value("label").Equal(name)

	//Delete
	e.DELETE(path+"/"+id).
		WithHeader("Authorization", token).Expect().Status(200)
	e.GET(path+"/"+id).
		WithHeader("Authorization", token).Expect().Status(404)
}
