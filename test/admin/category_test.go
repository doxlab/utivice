package test

import (
	"testing"

	"github.com/brianvoe/gofakeit/v5"
)

// TestCategory 测试树级分类管理功能应用服务
func TestCategory(t *testing.T) {

	_, server, e, auth := createTestServer(t)
	defer server.Close()

	token, err := auth.Login("admin", "123456")
	if err != nil {
		t.Fatal(err)
		return
	}
	token = "Bearer " + token
	path := "/category/system"

	name := gofakeit.Name()
	entity := map[string]interface{}{
		// TODO Change Next Code
		"name":        name,
		"group":       "abc",
		"code":        name,
		"parent":      "",
		"description": "description",
	}

	// POST
	result := e.POST(path).
		WithHeader("Authorization", token).
		WithJSON(entity).Expect().Status(200).
		JSON().Object()
	result.Value("id").NotNull()
	id := result.Value("id").Raw().(string)
	// TODO Change Equal Test
	result.Value("name").Equal(name)

	//add children
	for i := 0; i < 4; i++ {
		childname := name + "/" + gofakeit.Name()

		child := map[string]interface{}{
			// TODO Change Next Code
			"name":        childname,
			"group":       "abc",
			"code":        childname,
			"parent":      id,
			"description": "description",
		}
		result := e.POST(path).
			WithHeader("Authorization", token).
			WithJSON(child).Expect().Status(200).
			JSON().Object()
		result.Value("id").NotNull()
		result.Value("name").Equal(childname)

	}

	// Query
	array := e.GET(path).
		WithQuery("l", 10).
		WithHeader("Authorization", token).Expect().Status(200).
		JSON().Object().Value("data").Array()
	array.Length().Equal(1)
	// Query child

	array = e.GET(path).
		WithQuery("l", 10).
		WithQuery("parent", id).
		WithHeader("Authorization", token).Expect().Status(200).
		JSON().Object().Value("data").Array()
	array.Length().Equal(4)

	// Get
	result = e.GET(path+"/"+id).
		WithHeader("Authorization", token).Expect().Status(200).
		JSON().Object()
	result.Value("id").NotNull()
	// TODO Change Equal Test
	result.Value("name").Equal(name)
	//Put
	name = gofakeit.Name()
	entity["name"] = name
	result = e.PUT(path+"/"+id).
		WithHeader("Authorization", token).
		WithJSON(entity).Expect().Status(200).
		JSON().Object()
	result.Value("id").NotNull()
	id = result.Value("id").Raw().(string)
	// TODO Change Equal Test
	result.Value("name").Equal(name)

	//Delete
	e.DELETE(path+"/"+id).
		WithHeader("Authorization", token).Expect().Status(200)
	e.GET(path+"/"+id).
		WithHeader("Authorization", token).Expect().Status(404)
}
