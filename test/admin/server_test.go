package test

import (
	"log"
	"net/http/httptest"
	"path/filepath"
	"testing"

	client "gitee.com/doxlab/auth-rpc/clienttest"
	"gitee.com/doxlab/infrastructure/conf"
	"gitee.com/doxlab/infrastructure/repository"
	"gitee.com/doxlab/infrastructure/webserver"
	"gitee.com/doxlab/utivice/service"
	"gitee.com/doxlab/utivice/service/admin"
	"github.com/gavv/httpexpect"
)

// createTestServer 创建测试服务器
func createTestServer(t *testing.T) (s *webserver.Server, testserver *httptest.Server, expect *httpexpect.Expect, auth *client.AdminAuthClient) {

	var cfg struct {
		Mongodb   repository.DaoOption `yaml:"mongodb"`
		WebServer struct {
			Port       string `yaml:"port"`
			Cert       string `yaml:"cert"`
			Key        string `yaml:"key"`
			GzipEnable bool   `yaml:"gzip_enable"`
		} `yaml:"web_server"`
		AdminAuth struct {
			Host string `yaml:"host"`
			Port string `yaml:"port"`
		} `yaml:"adminAuth"`
	}

	cfg.Mongodb = repository.DaoOption{
		Addr:     "mongodb://localhost:27017",
		Database: "kernel",
	}

	if err := conf.LoadConfigFromYaml("res/conf/conf.yml", &cfg); err != nil {
		log.Fatal(err)
	}

	service.Repo(cfg.Mongodb)
	//content.InitContent(cfg.Mongodb)

	var cert string
	var key string
	if cfg.WebServer.Cert != "" && cfg.WebServer.Key != "" {
		cert = filepath.Join("res", "assets", "server", cfg.WebServer.Cert)
		key = filepath.Join("res", "assets", "server", cfg.WebServer.Key)
	}
	auth = client.NewAdminAuthClient("user", cfg.AdminAuth.Host, cfg.AdminAuth.Port)
	server := admin.NewServer(cert, key, cfg.WebServer.Port, cfg.WebServer.GzipEnable, auth)
	// TODO Init Custom Module
	admin.Init(server)
	testserver = httptest.NewServer(server.Server)

	expect = httpexpect.New(t, testserver.URL)
	return server, testserver, expect, auth
}

// TestServer 测试是否能启动服务器
func TestServer(t *testing.T) {

	_, server, _, auth := createTestServer(t)
	defer server.Close()
	// 登录

	token, err := auth.Login("admin", "123456")
	if err != nil {
		t.Fatal(err)
		return
	}
	token = "Bearer " + token

}
