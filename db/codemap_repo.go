package db

import (
	"context"

	"gitee.com/doxlab/deflib"
	"gitee.com/doxlab/infrastructure/repository"
	"gitee.com/doxlab/utivice/model"
)

// CodeMapRepo 定义代码表的存储实现
type CodeMapRepo struct {
	*repository.MongoRepo
}

// NewCodeMapRepo 创建代码表的存储实例
func NewCodeMapRepo(option repository.DaoOption) *CodeMapRepo {
	r, err := repository.NewMongoRepo(option)
	if err != nil {
		return nil
	}
	return &CodeMapRepo{
		MongoRepo: r,
	}
}

func (o *CodeMapRepo) GetBykey(key string) (code model.CodeMap, err error) {
	err = o.GetDao().
		Collection(model.CodeMapColl).
		FindOne(context.TODO(), deflib.M{"codeKey": key}).
		Decode(&code)
	return
}

// PushCode 添加代码项
func (o *CodeMapRepo) PushCode(id string, code model.CodeItem) error {
	_ = o.UpdateCmd(model.CodeMapColl, deflib.M{"id": id, "code": nil},
		deflib.M{"$set": deflib.M{"code": []interface{}{}}}, false)
	return o.UpdateCmd(model.CodeMapColl,
		deflib.M{"id": id, "code.value": deflib.M{"$ne": code.Value}},
		deflib.M{"$push": deflib.M{"code": code}}, false)
}

// PullCode 删除代码项
func (o *CodeMapRepo) PullCode(id string, oid string) error {
	return o.UpdateCmd(model.CodeMapColl,
		deflib.M{"id": id},
		deflib.M{"$pull": deflib.M{"code": deflib.M{"value": oid}}}, false)
}
