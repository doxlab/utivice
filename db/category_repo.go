package db

import (
	"gitee.com/doxlab/infrastructure/repository"
)

// CategoryRepo 定义公共分类的存储实现
type CategoryRepo struct {
	*repository.MongoRepo
}

// NewCategoryRepo 创建公共分类的存储实例
func NewCategoryRepo(option repository.DaoOption) *CategoryRepo {
	r, err := repository.NewMongoRepo(option)
	if err != nil {
		return nil
	}
	return &CategoryRepo{
		MongoRepo: r,
	}
}
