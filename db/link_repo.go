package db

import (
	"gitee.com/doxlab/infrastructure/repository"
)

// LinkRepo 定义链接管理功能的存储实现
type LinkRepo struct {
	*repository.MongoRepo
}

// NewLinkRepo 创建链接管理功能的存储实例
func NewLinkRepo(option repository.DaoOption) *LinkRepo {
	r, err := repository.NewMongoRepo(option)
	if err != nil {
		return nil
	}
	return &LinkRepo{
		MongoRepo: r,
	}
}
