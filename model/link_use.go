package model

import "gitee.com/doxlab/deflib"

// LinkColl 链接管理功能的存储表名
const LinkColl = "Link"

// ILinkRepo 链接管理功能存储接口
type ILinkRepo interface {
	deflib.ICommonRepo
	Count(coll string, filter deflib.M) (int64, error)
	UpdateField(coll string, id string, field string, value interface{}) error
}

// LinkUseCase  链接管理功能用例实现
type LinkUseCase struct {
	repo ILinkRepo
}

// NewLinkUseCase 创建链接管理功能的用例
func NewLinkUseCase() *LinkUseCase {
	uc := LinkUseCase{}
	r, err := Injector.Get(uc)
	if err != nil {
		return nil
	}
	if repo, ok := r.(ILinkRepo); !ok {
		return nil
	} else {
		uc.repo = repo
		return &uc
	}
}

// Create 创建新链接管理功能记录
func (uc *LinkUseCase) Create(r *Link) error {
	r.SetId(NewIdGenerator().NewId())
	r.SetDataMeta()
	return uc.repo.Create(r)
}

// Update 修改链接管理功能记录
func (uc *LinkUseCase) Update(r *Link) error {
	r.SetDataMeta()
	return uc.repo.Update(r)
}

// Delete 删除链接管理功能记录
func (uc *LinkUseCase) Delete(r *Link) error {
	return uc.repo.Delete(r)
}

// Get 根据指定的id获取链接管理功能记录
func (uc *LinkUseCase) Get(id string) (Link, error) {
	entity := Link{}
	entity.SetId(id)
	if err := uc.repo.Get(&entity); err != nil {
		return Link{}, err
	}
	return entity, nil
}

// Query 查询Link数据集
func (uc *LinkUseCase) Query(param deflib.QueryParam) ([]Link, int64, error) {
	data := make([]Link, 0, 0)
	param.Coll = LinkColl
	if total, err := uc.repo.Query(param, &data); err != nil {
		return nil, 0, err
	} else {
		return data, total, nil
	}
}

// SetaExtend 设置扩展属性
func (uc *LinkUseCase) SetExtend(id string, extend map[string]interface{}) error {
	return uc.repo.UpdateField(LinkColl, id, "extend", extend)
}
