package model

import "gitee.com/doxlab/deflib"

var Injector = deflib.NewRepoInjector()

// KnownError 已知的预定义的错误
type KnownError struct {
	Code string `bson:"code" json:"code"`
	S    string `bson:"s" json:"s"`
}

func (e *KnownError) Error() string {
	return e.S
}

// ErrorCode 获取错误编码
func (e *KnownError) ErrorCode() string {
	return e.Code
}

// NewError 创建已知错误
func NewError(code, err string) error {
	return &KnownError{code, err}
}

// 一般错误
var (
	ErrorEmptyID     = NewError("4001", "没有指定必须的ID")
	ErrorEmptyModule = NewError("4002", "没有指定必须的模块名称")
	ErrorNotAllow    = NewError("4101", "不允许操作")
	ErrorExist       = NewError("4102", "记录已经存在")
)
