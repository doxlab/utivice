package model

import (
	"net/http"

	"gitee.com/doxlab/deflib"
	"github.com/atomtree/binding"
)

// CategoryQL 公共分类查询对象
type CategoryQL struct {
	Skip   int64    `bson:"skip" json:"skip"`
	Limit  int64    `bson:"limit" json:"limit"`
	Sort   []string `bson:"sort" json:"sort"`
	Word   string   `bson:"word" json:"word" validate:"max=128"`
	Module string   `bson:"module" json:"module"`
	Parent string   `bson:"parent" json:"parent"`
}

func (o *CategoryQL) FieldMap(req *http.Request) binding.FieldMap {
	return binding.FieldMap{
		&o.Skip:   "s",
		&o.Limit:  "l",
		&o.Sort:   "o",
		&o.Word:   "k",
		&o.Parent: "parent",
	}
}

func (o *CategoryQL) Q() deflib.QueryParam {
	q := deflib.QueryParam{}
	q.Filter = o.M()
	q.Limit = o.Limit
	q.Skip = o.Skip
	q.Sort = o.Sort
	return q
}

// M 转换成查询参数
func (o *CategoryQL) M() map[string]interface{} {
	m := deflib.M{}

	if o.Word != "" {
		m["$or"] = []deflib.M{
			{"name": deflib.M{"$regex": ".*" + o.Word + ".*", "$options": "i"}},
			{"code": deflib.M{"$regex": ".*" + o.Word + ".*", "$options": "i"}},
		}
	}
	m["parent"] = o.Parent
	m["module"] = o.Module

	return m
}
