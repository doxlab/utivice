package model

import "gitee.com/doxlab/deflib"

// CodeMapColl 代码表的存储表名
const CodeMapColl = "CodeMap"

// ICodeMapRepo 代码表存储接口
type ICodeMapRepo interface {
	deflib.ICommonRepo
	Count(coll string, filter deflib.M) (int64, error)
	UpdateField(coll string, id string, field string, value interface{}) error
	GetBykey(key string) (code CodeMap, err error)
	PushCode(id string, code CodeItem) error
	PullCode(id string, oid string) error
}

// CodeMapUseCase  代码表用例实现
type CodeMapUseCase struct {
	repo ICodeMapRepo
}

// NewCodeMapUseCase 创建代码表的用例
func NewCodeMapUseCase() *CodeMapUseCase {
	uc := CodeMapUseCase{}
	r, err := Injector.Get(uc)
	if err != nil {
		return nil
	}
	if repo, ok := r.(ICodeMapRepo); !ok {
		return nil
	} else {
		uc.repo = repo
		return &uc
	}
}

// Create 创建新代码表记录
func (uc *CodeMapUseCase) Create(r *CodeMap) error {
	if uc.Exist(r.CodeKey) {
		return ErrorExist
	}
	r.SetId(NewIdGenerator().NewId())
	r.SetDataMeta()
	return uc.repo.Create(r)
}

// Exist 查询Key是否已经存在
func (uc *CodeMapUseCase) Exist(key string) bool {
	code, err := uc.repo.GetBykey(key)
	if err != nil {
		return false
	}
	if code.CodeKey == key {
		return true
	}
	return false

}

// Update 修改代码表记录
func (uc *CodeMapUseCase) Update(r *CodeMap) error {
	old, err := uc.GetBykey(r.CodeKey)
	if err != nil {
		return err
	}
	if old.ID != r.ID {
		return ErrorExist
	}
	r.SetDataMeta()
	return uc.repo.Update(r)
}

// Delete 删除代码表记录
func (uc *CodeMapUseCase) Delete(r *CodeMap) error {
	return uc.repo.Delete(r)
}

// Get 根据指定的id获取代码表记录
func (uc *CodeMapUseCase) Get(id string) (CodeMap, error) {
	entity := CodeMap{}
	entity.SetId(id)
	if err := uc.repo.Get(&entity); err != nil {
		return CodeMap{}, err
	}
	return entity, nil
}

// Get 根据指定的id获取代码表记录
func (uc *CodeMapUseCase) GetBykey(key string) (CodeMap, error) {
	return uc.repo.GetBykey(key)
}

// Query 查询CodeMap数据集
func (uc *CodeMapUseCase) Query(param deflib.QueryParam) ([]CodeMap, int64, error) {
	data := make([]CodeMap, 0, 0)
	param.Coll = CodeMapColl
	if total, err := uc.repo.Query(param, &data); err != nil {
		return nil, 0, err
	} else {
		return data, total, nil
	}
}

// PushCode 添加代码项
func (uc *CodeMapUseCase) PushCode(id string, code CodeItem) error {
	return uc.repo.PushCode(id, code)
}

// PullCode  删除代码项
func (uc *CodeMapUseCase) PullCode(id string, oid string) error {
	return uc.repo.PullCode(id, oid)
}
