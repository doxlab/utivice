package model

import (
	"gitee.com/doxlab/deflib"
)

//CodeMap 代码表数据项
// Model: CodeMap
// Title: 代码表
// Path: code
// Doc:
type CodeMap struct {
	ID              string `bson:"id" json:"id"`
	deflib.DataMeta `json:",inline" bson:",inline"`
	CodeKey         string     `bson:"codeKey" json:"codeKey" yaml:"codeKey"`             //代码表编码
	Name            string     `bson:"name" json:"name" yaml:"name"`                      //代码表名称
	Description     string     `bson:"description" json:"description" yaml:"description"` // 说明
	Code            []CodeItem `bson:"code" json:"code" yaml:"code" handle:"+"`           // 代码项
}

// CodeItem 代码项
// Title: 代码项
// Doc:
type CodeItem struct {
	Label   string `bson:"label" json:"label" yaml:"label"`
	Value   string `bson:"value" json:"value" yaml:"value"`
	Summary string `bson:"summary" json:"summary" yaml:"summary"`
}

// SetId 设置ID
func (o *CodeMap) SetId(id string) {
	o.ID = id
}

// GetId 获取ID
func (o *CodeMap) GetId() string {
	return o.ID
}
