package model

import (
	"gitee.com/doxlab/deflib"
)

// Category 公共分类
// Model: Category
// Title: 公共分类
// Doc: 提供公共的分类功能，可以供整个系统使用。对于需要有分类的功能的应用模块，不需要自己开发一个分类功能。
// 只需要引用关联本功能模块的分类即可
type Category struct {
	ID              string `bson:"id" json:"id"`
	deflib.DataMeta `json:",inline" bson:",inline"`
	Module          string      `bson:"module" json:"module" yaml:"module"`                //关联的功能模块
	Group           string      `bson:"group" json:"group" yaml:"group"`                   //分组
	Name            string      `bson:"name" json:"name" yaml:"name"`                      //分类名称
	Code            string      `bson:"code" json:"code" yaml:"code"`                      //分类编码
	Description     string      `bson:"description" json:"description" yaml:"description"` //说明
	Parent          string      `bson:"parent" json:"parent" yaml:"parent" handle:"+"`     //上级分类
	CatType         string      `bson:"catType" json:"catType"`
	Properties      interface{} `bson:"properties" json:"properties" yaml:"properties" handle:"+"` //扩展属性
	// Path            []*CategoryInfo `bson:"-" json:"path"`
}
type CategoryInfo struct {
	ID   string `bson:"id" json:"id"`
	Name string `bson:"name" json:"name" yaml:"name"` //分类名称
	Code string `bson:"code" json:"code" yaml:"code"` //分类编码
}

// SetId 设置ID
func (o *Category) SetId(id string) {
	o.ID = id
}

// GetId 获取ID
func (o *Category) GetId() string {
	return o.ID
}

// func (o *Category) AppendPath(path []*CategoryInfo) {
// 	o.Path = append(path, &CategoryInfo{ID: o.ID, Name: o.Name, Code: o.Code})
// }
