package model

import (
	"net/http"

	"gitee.com/doxlab/deflib"
	"github.com/atomtree/binding"
)

// LinkQL 链接管理功能查询对象
type LinkQL struct {
	Skip  int64    `bson:"skip" json:"skip"`
	Limit int64    `bson:"limit" json:"limit"`
	Sort  []string `bson:"sort" json:"sort"`
	Word  string   `bson:"word" json:"word" validate:"max=128"`
	Tags  []string `bson:"tag" json:"tag"`
}

func (o *LinkQL) FieldMap(req *http.Request) binding.FieldMap {
	return binding.FieldMap{
		&o.Skip:  "s",
		&o.Limit: "l",
		&o.Sort:  "o",
		&o.Word:  "k",
	}
}

func (o *LinkQL) Q() deflib.QueryParam {
	q := deflib.QueryParam{}
	q.Filter = o.M()
	q.Limit = o.Limit
	if q.Limit <= 0 {
		q.Limit = 30
	}
	q.Skip = o.Skip
	q.Sort = o.Sort
	return q
}

// M 转换成查询参数
func (o *LinkQL) M() map[string]interface{} {
	m := deflib.M{}

	if o.Word != "" {
		m["$or"] = []deflib.M{
			{"label": deflib.M{"$regex": ".*" + o.Word + ".*", "$options": "i"}},
			{"url": deflib.M{"$regex": ".*" + o.Word + ".*", "$options": "i"}},
		}
	}
	if len(o.Tags) > 0 {
		m["tags"] = deflib.M{
			"$in": o.Tags,
		}
	}

	return m
}
