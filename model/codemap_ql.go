package model

import (
	"net/http"

	"gitee.com/doxlab/deflib"
	"github.com/atomtree/binding"
)

// CodeMapQL 代码表查询对象
type CodeMapQL struct {
	Skip  int64    `bson:"skip" json:"skip"`
	Limit int64    `bson:"limit" json:"limit"`
	Sort  []string `bson:"sort" json:"sort"`
	Word  string   `bson:"word" json:"word" validate:"max=128"`
	Key   string   `bson:"key" json:"key"`
}

func (o *CodeMapQL) FieldMap(req *http.Request) binding.FieldMap {
	return binding.FieldMap{
		&o.Skip:  "s",
		&o.Limit: "l",
		&o.Sort:  "o",
		&o.Word:  "k",
		&o.Key:   "key",
	}
}

func (o *CodeMapQL) Q() deflib.QueryParam {
	q := deflib.QueryParam{}
	q.Filter = o.M()
	q.Limit = o.Limit
	if q.Limit <= 0 {
		q.Limit = 30
	}
	q.Skip = o.Skip
	q.Sort = o.Sort
	return q
}

// M 转换成查询参数
func (o *CodeMapQL) M() map[string]interface{} {
	m := deflib.M{}

	if o.Word != "" {
		m["$or"] = []deflib.M{
			{"name": deflib.M{"$regex": ".*" + o.Word + ".*", "$options": "i"}},
			{"codeKey": deflib.M{"$regex": ".*" + o.Word + ".*", "$options": "i"}},
		}
	}
	if o.Key != "" {
		m["codeKey"] = o.Key
	}

	return m
}
