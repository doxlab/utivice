package model

import (
	"errors"

	"gitee.com/doxlab/deflib"
)

// CategoryColl 公共分类的存储表名
const CategoryColl = "Category"

// ICategoryRepo 公共分类存储接口
type ICategoryRepo interface {
	deflib.ICommonRepo
	Count(coll string, filter deflib.M) (int64, error)
	UpdateField(coll string, id string, field string, value interface{}) error
}

// CategoryUseCase  公共分类用例实现
type CategoryUseCase struct {
	repo ICategoryRepo
}

// NewCategoryUseCase 创建公共分类的用例
func NewCategoryUseCase() *CategoryUseCase {
	uc := CategoryUseCase{}
	r, err := Injector.Get(uc)
	if err != nil {
		return nil
	}
	if repo, ok := r.(ICategoryRepo); !ok {
		return nil
	} else {
		uc.repo = repo
		return &uc
	}
}

// Create 创建新公共分类记录
func (uc *CategoryUseCase) Create(r *Category) error {
	r.SetId(NewIdGenerator().NewId())
	r.SetDataMeta()
	return uc.repo.Create(r)
}

// Update 修改公共分类记录
func (uc *CategoryUseCase) Update(r *Category) error {
	old, err := uc.Get(r.ID)
	if err != nil {
		return err
	}
	r.Module = old.Module
	r.CreateTime = old.CreateTime
	r.SetDataMeta()
	return uc.repo.Update(r)
}

// Delete 删除公共分类记录
func (uc *CategoryUseCase) Delete(r *Category) error {
	c, err := uc.repo.Count(CategoryColl, deflib.M{"parent": r.ID})
	if err != nil {
		return err
	}
	if c > 0 {
		return errors.New("children not empty")
	}
	return uc.repo.Delete(r)
}

// Get 根据指定的id获取公共分类记录
func (uc *CategoryUseCase) Get(id string) (Category, error) {
	entity := Category{}
	entity.SetId(id)
	if err := uc.repo.Get(&entity); err != nil {
		return Category{}, err
	}
	return entity, nil
}

// Query 查询Category数据集
func (uc *CategoryUseCase) Query(param deflib.QueryParam) ([]Category, int64, error) {
	data := make([]Category, 0, 0)
	param.Coll = CategoryColl
	if total, err := uc.repo.Query(param, &data); err != nil {
		return nil, 0, err
	} else {
		return data, total, nil
	}
}

//GetPath 获取路径节点对象
//返回一个数组，第一个元素是第一级，最后一个元素是id所指的分类节点
func (uc *CategoryUseCase) GetPath(id string) ([]*CategoryInfo, error) {

	path := make([]*CategoryInfo, 0)
	current, err := uc.Get(id)
	if err != nil {
		return nil, err
	}
	path = append(path, &CategoryInfo{ID: current.ID, Name: current.Name, Code: current.Code})
	for i := 0; i < 100; i++ { //避免死循环，最多100级
		if current.Parent == "" {
			break
		}
		current, err = uc.Get(current.Parent)
		if err != nil {
			return nil, err
		}
		path = append(path, &CategoryInfo{ID: current.ID, Name: current.Name, Code: current.Code})
	}

	// revert
	for i := 0; i < len(path)/2; i++ {
		j := len(path) - i - 1
		path[i], path[j] = path[j], path[i]
	}
	return path, nil
}

// SetaParent 设置上级分类
func (uc *CategoryUseCase) SetParent(id string, parent string) error {
	return uc.repo.UpdateField(CategoryColl, id, "parent", parent)
}

// SetaProperties 设置扩展属性
func (uc *CategoryUseCase) SetProperties(id string, properties interface{}) error {
	return uc.repo.UpdateField(CategoryColl, id, "properties", properties)
}
