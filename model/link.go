package model

import (
	"gitee.com/doxlab/deflib"
	"go.mongodb.org/mongo-driver/bson/primitive"
)

type IdGenerator interface {
	NewId() string
}

type BsonIdGenerator struct {
}

// NewIdGenerator 生成ID的对象
func NewIdGenerator() IdGenerator {
	return &BsonIdGenerator{}
}

func (g *BsonIdGenerator) NewId() string {
	return primitive.NewObjectID().Hex()
}

// Link 链接管理
// Model: Link
// Title: 链接管理功能
// Doc: 提供一个公共的功能，管理系统的链接
type Link struct {
	ID              string                 `bson:"id" json:"id"`
	Label           string                 `bson:"label" json:"label" yaml:"label"`                   //链接的名称
	IconCls         string                 `bson:"iconCls" json:"iconCls" yaml:"iconCls"`             //图标的样式类名
	Image           string                 `bson:"image" json:"image" yaml:"image"`                   // 图片图标
	Url             string                 `bson:"url" json:"url" yaml:"url"`                         //地址
	Tags            []string               `bson:"tags" json:"tags" yaml:"tags" handle:"+"`           //标签
	Extend          map[string]interface{} `bson:"extend" json:"extend" yaml:"extend" handle:"+"`     //扩展属性
	Description     string                 `bson:"description" json:"description" yaml:"description"` //说明
	deflib.DataMeta `json:",inline" bson:",inline"`
}

// SetId 设置ID
func (o *Link) SetId(id string) {
	o.ID = id
}

// GetId 获取ID
func (o *Link) GetId() string {
	return o.ID
}
